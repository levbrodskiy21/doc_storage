package com.lva.documentstorage.request;

import lombok.Data;

@Data
public class CatalogRequest {
    private String title;
    private String description;
}
