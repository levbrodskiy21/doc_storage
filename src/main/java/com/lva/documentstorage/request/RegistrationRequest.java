package com.lva.documentstorage.request;

import lombok.Data;

@Data
public class RegistrationRequest {
    private String email;
    private String password;
}
