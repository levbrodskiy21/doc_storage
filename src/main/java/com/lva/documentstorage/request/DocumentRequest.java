package com.lva.documentstorage.request;

import com.lva.documentstorage.domain.Category;
import com.lva.documentstorage.domain.enums.CategoryName;
import lombok.Data;

@Data
public class DocumentRequest {
    private String path;
    private String name;
    private String description;
}
