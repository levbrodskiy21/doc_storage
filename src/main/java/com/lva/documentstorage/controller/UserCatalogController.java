package com.lva.documentstorage.controller;

import com.lva.documentstorage.Main;
import com.lva.documentstorage.domain.Catalog;
import com.lva.documentstorage.service.CatalogService;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.io.IOException;

@Controller
public class UserCatalogController {
    @FXML
    private TableView<Catalog> table;

    private final CatalogService catalogService;

    @Autowired
    public UserCatalogController(CatalogService catalogService) {
        this.catalogService = catalogService;
    }

    @FXML
    public void initialize() {
        updateTable();
    }

    @FXML
    void back(ActionEvent event) throws IOException {
        AuthenticationController.load();
    }

    void updateTable() {
        TableColumn<Catalog, Catalog> idCol = new TableColumn<>("ID");
        idCol.setCellValueFactory(new PropertyValueFactory<>("id"));

        TableColumn<Catalog, Catalog> nameCol = new TableColumn<>("Название");
        nameCol.setCellValueFactory(new PropertyValueFactory<>("title"));

        TableColumn<Catalog, Catalog> descriptionCol = new TableColumn<>("Описание");
        descriptionCol.setCellValueFactory(new PropertyValueFactory<>("description"));

        TableColumn<Catalog, Catalog> dateCol = new TableColumn<>("Дата регистрации");
        dateCol.setCellValueFactory(new PropertyValueFactory<>("createdAt"));

        TableColumn<Catalog, Catalog> open = new TableColumn<>("");
        open.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue()));
        open.setCellFactory(param -> new TableCell<Catalog, Catalog>() {
            private final Button openButton = new Button("Открыть");

            @Override
            protected void updateItem(Catalog catalog, boolean empty) {
                super.updateItem(catalog, empty);

                if (catalog == null) {
                    setGraphic(null);
                    return;
                }

                setGraphic(openButton);
                openButton.setOnAction(event -> {
                    try {
                        catalogService.setCurrentCatalog(catalog);
                        UserDocumentList.load();
                    } catch (Exception e) {
                        e.printStackTrace();
                        PopupWindow.openWindow("Ошибка", "Запись не найдена");
                    }
                });
            }
        });

        ObservableList<Catalog> values = FXCollections
                .observableArrayList(catalogService.getCatalogs());

        table.setItems(values);
        table.getColumns().clear();

        table.getColumns().addAll(
                idCol, nameCol,
                descriptionCol,
                dateCol, open
        );
    }

    public static void load() throws IOException {
        FXMLLoader loader = new FXMLLoader(RegistrationController.class.getResource("/user_catalogs.fxml"));
        loader.setControllerFactory(Main.getApplicationContext().getBeanFactory()::getBean);
        Parent view = loader.load();
        Main.getStage().setScene(new Scene(view));
        Main.getStage().show();
    }
}
