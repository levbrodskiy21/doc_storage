package com.lva.documentstorage.controller;

import lombok.Data;

@Data
public class DocumentPermission {
    private Long permissionId;
    private Long userId;
    private String userName;
    private Long documentId;
    private String documentName;
    private Boolean accepted;
}
