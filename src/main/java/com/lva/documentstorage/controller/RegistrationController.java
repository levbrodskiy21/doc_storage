package com.lva.documentstorage.controller;

import com.lva.documentstorage.Main;
import com.lva.documentstorage.domain.enums.RoleName;
import com.lva.documentstorage.service.UserService;
import com.lva.documentstorage.service.exception.EntityAlreadyExistsException;
import com.lva.documentstorage.service.exception.EntityNotFoundException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;

import java.io.IOException;

@Controller
public class RegistrationController {
    private final UserService userService;

    @FXML
    private TextField email;

    @FXML
    private TextField password;

    @Autowired
    public RegistrationController(UserService userService) {
        this.userService = userService;
    }

    @FXML
    void authButtonClick(ActionEvent event) throws IOException {
        AuthenticationController.load();
    }

    @FXML
    void registrationButtonClick(ActionEvent event) throws IOException {
        String login = email.getText();
        String pass = password.getText();

        if (StringUtils.isEmpty(login) || StringUtils.isEmpty(pass)) {
            PopupWindow.openWindow("isEmpty", "isEmpty");
            return;
        }

        try {
            userService.register(login, pass);

            RoleName roleName = userService.getCurrentUser().getRole().getName();

            if (roleName == RoleName.USER) {
                UserCatalogController.load();
            } else if (roleName == RoleName.ADMIN) {
                AdminCatalogController.load();
            }
        } catch (EntityAlreadyExistsException e) {
            e.printStackTrace();
            PopupWindow.openWindow( "Пользователь уже существует");
        }
    }

    public static void load() throws IOException {
        FXMLLoader loader = new FXMLLoader(RegistrationController.class.getResource("/registration.fxml"));
        loader.setControllerFactory(Main.getApplicationContext().getBeanFactory()::getBean);
        Parent view = loader.load();
        Main.getStage().setScene(new Scene(view));
        Main.getStage().show();
    }
}
