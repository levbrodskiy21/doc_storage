package com.lva.documentstorage.controller;

import com.lva.documentstorage.Main;
import com.lva.documentstorage.request.DocumentRequest;
import com.lva.documentstorage.service.CatalogService;
import com.lva.documentstorage.service.DocumentService;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.IOException;

@Controller
public class AddDocumentController {
    private final DocumentService documentService;
    private final CatalogService catalogService;
    private FileChooser fileChooser;
    private DocumentRequest documentRequest = new DocumentRequest();

    @FXML
    private TextField title;

    @FXML
    private TextArea desc;

    @FXML
    private Label fileName;

    @Autowired
    public AddDocumentController(DocumentService documentService, CatalogService catalogService) {
        this.documentService = documentService;
        this.catalogService = catalogService;
    }

    public static void load() throws IOException {
        FXMLLoader loader = new FXMLLoader(RegistrationController.class.getResource("/add_document.fxml"));
        loader.setControllerFactory(Main.getApplicationContext().getBeanFactory()::getBean);
        Parent view = loader.load();
        Main.getStage().setScene(new Scene(view));
        Main.getStage().show();
    }

    @FXML
    public void initialize() {
        fileChooser = new FileChooser();
        fileChooser.setInitialFileName("file");
        fileChooser.setTitle("Выберите файл");
    }

    @FXML
    void add(ActionEvent event) {
        if (StringUtils.isEmpty(title.getText()) || StringUtils.isEmpty(desc.getText())) {
            PopupWindow.openWindow("Название или описание не заполнено");
            return;
        }

        if (StringUtils.isEmpty(documentRequest.getPath())) {
            PopupWindow.openWindow("Файл не выбран");
            return;
        }

        documentRequest.setName(title.getText());
        documentRequest.setDescription(desc.getText());

        try {
            documentService.addDocument(catalogService.getCurrentCatalog(), documentRequest);
            title.setText("");
            desc.setText("");
            fileName.setText("");
            fileChooser = new FileChooser();
        } catch (IOException e) {
            PopupWindow.openWindow("Файла не существует");
        }
    }

    @FXML
    void back(ActionEvent event) throws IOException {
        AdminDocumentListController.load();
    }

    @FXML
    void chooseFile(ActionEvent event) {
        File file = fileChooser.showOpenDialog(Main.getStage());

        if (file != null) {
            fileName.setText(file.getName());
            documentRequest.setPath(file.getPath());
        }
    }
}
