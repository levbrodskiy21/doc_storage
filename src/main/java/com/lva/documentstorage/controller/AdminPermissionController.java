package com.lva.documentstorage.controller;

import com.lva.documentstorage.Main;
import com.lva.documentstorage.service.DocumentService;
import com.lva.documentstorage.service.PermissionService;
import com.lva.documentstorage.service.UserService;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import org.springframework.stereotype.Controller;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class AdminPermissionController {
    private final PermissionService permissionService;
    private final UserService userService;
    private final DocumentService documentService;

    @FXML
    private TableView<DocumentPermission> table;

    public AdminPermissionController(PermissionService permissionService, UserService userService, DocumentService documentService) {
        this.permissionService = permissionService;
        this.userService = userService;
        this.documentService = documentService;
    }

    @FXML
    public void initialize() {
        updateTable();
    }

    @FXML
    void back(ActionEvent event) throws IOException {
        AdminCatalogController.load();
    }

    void updateTable() {
        TableColumn<DocumentPermission, DocumentPermission> userIdCol = new TableColumn<>("ID Пользователя");
        userIdCol.setCellValueFactory(new PropertyValueFactory<>("userId"));

        TableColumn<DocumentPermission, DocumentPermission> userNameCol = new TableColumn<>("Имя Пользователя");
        userNameCol.setCellValueFactory(new PropertyValueFactory<>("userName"));

        TableColumn<DocumentPermission, DocumentPermission> documentIdCol = new TableColumn<>("ID Документа");
        documentIdCol.setCellValueFactory(new PropertyValueFactory<>("documentId"));

        TableColumn<DocumentPermission, DocumentPermission> documentNameCol = new TableColumn<>("Название Документа");
        documentNameCol.setCellValueFactory(new PropertyValueFactory<>("documentName"));

        TableColumn<DocumentPermission, DocumentPermission> acceptCol = new TableColumn<>("");
        acceptCol.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue()));
        acceptCol.setCellFactory(param -> new TableCell<DocumentPermission, DocumentPermission>() {
            private final Button button = new Button("Дать доступ");

            @Override
            protected void updateItem(DocumentPermission item, boolean empty) {
                super.updateItem(item, empty);
                System.out.println(item);
                if (item == null) {
                    setGraphic(null);
                    return;
                }

                if (item.getAccepted()) {
                    setGraphic(null);
                    return;
                }

                setGraphic(button);
                button.setOnAction(event -> {
                    try {
                        permissionService.acceptPermission(item.getPermissionId());
                        updateTable();
                    } catch (Exception e) {
                        PopupWindow.openWindow("Ошибка", "Запись не найдена");
                    }
                });
            }
        });

        TableColumn<DocumentPermission, DocumentPermission> deniedCol = new TableColumn<>("");
        deniedCol.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue()));
        deniedCol.setCellFactory(param -> new TableCell<DocumentPermission, DocumentPermission>() {
            private final Button button = new Button("Снять Доступ");

            @Override
            protected void updateItem(DocumentPermission item, boolean empty) {
                super.updateItem(item, empty);

                if (item == null) {
                    setGraphic(null);
                    return;
                }
                if (!item.getAccepted()) {
                    setGraphic(null);
                    return;
                }
                setGraphic(button);
                button.setOnAction(event -> {
                    try {
                        permissionService.deniedPermission(item.getPermissionId());
                        updateTable();
                    } catch (Exception e) {
                        PopupWindow.openWindow("Ошибка", "Запись не найдена");
                    }
                });
            }
        });

        List<DocumentPermission> documentPermissionList = permissionService.getPermissions().stream()
                .map(p -> {
                    DocumentPermission dp = new DocumentPermission();
                    dp.setPermissionId(p.getId());
                    dp.setUserId(p.getUser().getId());
                    dp.setUserName(p.getUser().getEmail());
                    dp.setDocumentId(p.getDocument().getId());
                    dp.setDocumentName(p.getDocument().getName());
                    dp.setAccepted(p.getAccepted());
                    return dp;
                }).collect(Collectors.toList());

        ObservableList<DocumentPermission> values = FXCollections
                .observableArrayList(documentPermissionList);

        table.setItems(values);
        table.getColumns().clear();

        table.getColumns().addAll(
                userIdCol, userNameCol,
                documentIdCol,
                documentNameCol, acceptCol, deniedCol
        );
    }

    public static void load() throws IOException {
        FXMLLoader loader = new FXMLLoader(RegistrationController.class.getResource("/admin_permissions.fxml"));
        loader.setControllerFactory(Main.getApplicationContext().getBeanFactory()::getBean);
        Parent view = loader.load();
        Main.getStage().setScene(new Scene(view));
        Main.getStage().show();
    }
}

