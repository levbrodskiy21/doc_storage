package com.lva.documentstorage.controller;

import com.lva.documentstorage.Main;
import com.lva.documentstorage.domain.Catalog;
import com.lva.documentstorage.request.CatalogRequest;
import com.lva.documentstorage.service.CatalogService;
import com.lva.documentstorage.service.exception.EntityNotFoundException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;

import java.io.IOException;

@Controller
public class EditCatalogController {
    private final CatalogService catalogService;
    @FXML
    private TextField title;

    @FXML
    private TextArea description;

    public EditCatalogController(CatalogService catalogService) {
        this.catalogService = catalogService;
    }

    @FXML
    public void initialize() {
        Catalog catalog = catalogService.getCurrentCatalog();
        title.setText(catalog.getTitle());
        description.setText(catalog.getDescription());
    }

    @FXML
    void back(ActionEvent event) throws IOException {
        AdminCatalogController.load();
    }

    @FXML
    void edit(ActionEvent event) {
        if (StringUtils.isEmpty(title.getText()) || StringUtils.isEmpty(description.getText())) {
            PopupWindow.openWindow("");
        }

        CatalogRequest request = new CatalogRequest();
        request.setTitle(title.getText());
        request.setDescription(description.getText());

        try {
            catalogService.updateCatalog(catalogService.getCurrentCatalog().getId(), request);
        } catch (EntityNotFoundException e) {
            PopupWindow.openWindow("Каталог не найден");
        }
    }

    public static void load() throws IOException {
        FXMLLoader loader = new FXMLLoader(RegistrationController.class.getResource("/edit_catalog.fxml"));
        loader.setControllerFactory(Main.getApplicationContext().getBeanFactory()::getBean);
        Parent view = loader.load();
        Main.getStage().setScene(new Scene(view));
        Main.getStage().show();
    }
}
