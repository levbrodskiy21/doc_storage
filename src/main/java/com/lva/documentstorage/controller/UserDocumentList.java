package com.lva.documentstorage.controller;

import com.lva.documentstorage.Main;
import com.lva.documentstorage.domain.Document;
import com.lva.documentstorage.domain.PermissionRequest;
import com.lva.documentstorage.service.CatalogService;
import com.lva.documentstorage.service.DocumentService;
import com.lva.documentstorage.service.PermissionService;
import com.lva.documentstorage.service.UserService;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import org.springframework.stereotype.Controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@Controller
public class UserDocumentList {
    private final CatalogService catalogService;
    private final DocumentService documentService;
    private final PermissionService permissionService;
    private final UserService userService;

    @FXML
    private TableView<Document> table;

    public UserDocumentList(CatalogService catalogService, DocumentService documentService, PermissionService permissionService, UserService userService) {
        this.catalogService = catalogService;
        this.documentService = documentService;
        this.permissionService = permissionService;
        this.userService = userService;
    }

    @FXML
    void back(ActionEvent event) throws IOException {
        UserCatalogController.load();
    }

    @FXML
    public void initialize() {
        updateTable();
    }

    void updateTable() {
        TableColumn<Document, Document> idCol = new TableColumn<>("ID");
        idCol.setCellValueFactory(new PropertyValueFactory<>("id"));

        TableColumn<Document, Document> nameCol = new TableColumn<>("Название");
        nameCol.setCellValueFactory(new PropertyValueFactory<>("name"));

        TableColumn<Document, Document> descriptionCol = new TableColumn<>("Описание");
        descriptionCol.setCellValueFactory(new PropertyValueFactory<>("description"));

        TableColumn<Document, Document> dateCol = new TableColumn<>("Дата регистрации");
        dateCol.setCellValueFactory(new PropertyValueFactory<>("createdAt"));

        TableColumn<Document, Document> permissionCol = new TableColumn<>("");
        permissionCol.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue()));
        permissionCol.setCellFactory(param -> new TableCell<Document, Document>() {
            private final Button openButton = new Button("Получить доступ");

            @Override
            protected void updateItem(Document item, boolean empty) {
                super.updateItem(item, empty);

                if (item == null) {
                    setGraphic(null);
                    return;
                }

                if (permissionService.permissionExists(
                        userService.getCurrentUser().getId(),
                        item.getId())) {
                    setGraphic(null);
                    return;
                }

                setGraphic(openButton);
                openButton.setOnAction(event -> {
                    try {
                        PermissionRequest request = new PermissionRequest();
                        request.setDocument(item);
                        request.setUser(userService.getCurrentUser());
                        permissionService.addPermissionRequest(request);
                        updateTable();
                    } catch (Exception e) {
                        PopupWindow.openWindow("Ошибка", "Запись не найдена");
                    }
                });
            }
        });

        TableColumn<Document, Document> open = new TableColumn<>("");
        open.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue()));
        open.setCellFactory(param -> new TableCell<Document, Document>() {
            private final Button openButton = new Button("Скачать");

            @Override
            protected void updateItem(Document item, boolean empty) {
                super.updateItem(item, empty);

                if (item == null) {
                    setGraphic(null);
                    return;
                }

                if (!permissionService.permissionExists(
                        userService.getCurrentUser().getId(),
                        item.getId())) {
                    setGraphic(null);
                    return;
                }

                setGraphic(openButton);
                openButton.setOnAction(event -> {
                    try {
                        DirectoryChooser chooser = new DirectoryChooser();
                        chooser.setTitle("Выберите папку");
                        File file = chooser.showDialog(Main.getStage());

                        if (file != null) {
                            if (!file.isDirectory()) {
                                PopupWindow.openWindow("Не является папкой");
                                return;
                            }

                            String savePath = file.getAbsoluteFile() + File.separator + item.getName() + "." + item.getExtension();

                            Files.write(Paths.get(savePath), item.getData());

                            PopupWindow.openWindow("Готово", "Готово");
                        }
                    } catch (Exception e) {
                        PopupWindow.openWindow("Ошибка", "Запись не найдена");
                    }
                });
            }
        });

        ObservableList<Document> values = FXCollections
                .observableArrayList(documentService.getDocuments(catalogService.getCurrentCatalog()));

        table.setItems(values);
        table.getColumns().clear();

        table.getColumns().addAll(
                idCol, nameCol,
                descriptionCol,
                dateCol, permissionCol, open
        );
    }

    public static void load() throws IOException {
        FXMLLoader loader = new FXMLLoader(RegistrationController.class.getResource("/user_documents.fxml"));
        loader.setControllerFactory(Main.getApplicationContext().getBeanFactory()::getBean);
        Parent view = loader.load();
        Main.getStage().setScene(new Scene(view));
        Main.getStage().show();
    }
}
