package com.lva.documentstorage.controller;

import com.lva.documentstorage.Main;
import com.lva.documentstorage.domain.Catalog;
import com.lva.documentstorage.request.CatalogRequest;
import com.lva.documentstorage.service.CatalogService;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.io.IOException;

@Controller
public class AdminCatalogController {
    @FXML
    private TableView<Catalog> table;

    @FXML
    private TextField title;

    @FXML
    private TextArea description;

    private final CatalogService catalogService;

    @Autowired
    public AdminCatalogController(CatalogService catalogService) {
        this.catalogService = catalogService;
    }

    @FXML
    void add(ActionEvent event) {
        if (title.getText().length() < 1) {
            PopupWindow.openWindow("", "");
            return;
        }

        if (description.getText().length() < 1) {
            PopupWindow.openWindow("", "");
            return;
        }

        CatalogRequest catalogRequest = new CatalogRequest();
        catalogRequest.setTitle(title.getText());
        catalogRequest.setDescription(description.getText());

        catalogService.addCatalog(catalogRequest);

        title.setText("");
        description.setText("");

        updateTable();
    }

    @FXML
    void back(ActionEvent event) throws IOException {
        AuthenticationController.load();
    }

    @FXML
    void permissions(ActionEvent event) throws IOException {
        AdminPermissionController.load();
    }

    @FXML
    public void initialize() {
        updateTable();
    }

    void updateTable() {
        TableColumn<Catalog, Catalog> idCol = new TableColumn<>("ID");
        idCol.setCellValueFactory(new PropertyValueFactory<>("id"));

        TableColumn<Catalog, Catalog> nameCol = new TableColumn<>("Название");
        nameCol.setCellValueFactory(new PropertyValueFactory<>("title"));

        TableColumn<Catalog, Catalog> descriptionCol = new TableColumn<>("Описание");
        descriptionCol.setCellValueFactory(new PropertyValueFactory<>("description"));

        TableColumn<Catalog, Catalog> dateCol = new TableColumn<>("Дата регистрации");
        dateCol.setCellValueFactory(new PropertyValueFactory<>("createdAt"));

        TableColumn<Catalog, Catalog> delete = new TableColumn<>("");
        delete.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue()));
        delete.setCellFactory(param -> new TableCell<Catalog, Catalog>() {
            private final Button deleteButton = new Button("Удалить");

            @Override
            protected void updateItem(Catalog catalog, boolean empty) {
                super.updateItem(catalog, empty);

                if (catalog == null) {
                    setGraphic(null);
                    return;
                }

                setGraphic(deleteButton);
                deleteButton.setOnAction(event -> {
                    try {
                        catalogService.deleteCatalog(catalog.getId());
                        updateTable();
                    } catch (Exception e) {
                        e.printStackTrace();
                        PopupWindow.openWindow("Ошибка", "Запись не найдена");
                    }
                });
            }
        });

        TableColumn<Catalog, Catalog> open = new TableColumn<>("");
        open.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue()));
        open.setCellFactory(param -> new TableCell<Catalog, Catalog>() {
            private final Button button = new Button("Открыть");

            @Override
            protected void updateItem(Catalog catalog, boolean empty) {
                super.updateItem(catalog, empty);

                if (catalog == null) {
                    setGraphic(null);
                    return;
                }

                setGraphic(button);
                button.setOnAction(event -> {
                    try {
                        catalogService.setCurrentCatalog(catalog);
                        AdminDocumentListController.load();
                    } catch (Exception e) {
                        e.printStackTrace();
                        PopupWindow.openWindow("Ошибка", "Запись не найдена");
                    }
                });
            }
        });

        TableColumn<Catalog, Catalog> edit = new TableColumn<>("");
        edit.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue()));
        edit.setCellFactory(param -> new TableCell<Catalog, Catalog>() {
            private final Button button = new Button("Редактировать");

            @Override
            protected void updateItem(Catalog catalog, boolean empty) {
                super.updateItem(catalog, empty);

                if (catalog == null) {
                    setGraphic(null);
                    return;
                }

                setGraphic(button);
                button.setOnAction(event -> {
                    try {
                        catalogService.setCurrentCatalog(catalog);
                        EditCatalogController.load();
                    } catch (Exception e) {
                        e.printStackTrace();
                        PopupWindow.openWindow("Ошибка", "Запись не найдена");
                    }
                });
            }
        });

        ObservableList<Catalog> values = FXCollections
                .observableArrayList(catalogService.getCatalogs());

        table.setItems(values);
        table.getColumns().clear();

        table.getColumns().addAll(
                idCol, nameCol,
                descriptionCol,
                dateCol, open,
                edit, delete
        );
    }

    public static void load() throws IOException {
        FXMLLoader loader = new FXMLLoader(RegistrationController.class.getResource("/admin_catalogs.fxml"));
        loader.setControllerFactory(Main.getApplicationContext().getBeanFactory()::getBean);
        Parent view = loader.load();
        Main.getStage().setScene(new Scene(view));
        Main.getStage().show();
    }
}
