package com.lva.documentstorage.controller;

import com.lva.documentstorage.Main;
import com.lva.documentstorage.domain.Document;
import com.lva.documentstorage.service.CatalogService;
import com.lva.documentstorage.service.DocumentService;
import com.lva.documentstorage.service.UserService;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.DirectoryChooser;
import org.springframework.stereotype.Controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@Controller
public class AdminDocumentListController {
    private final DocumentService documentService;
    private final CatalogService catalogService;

    @FXML
    private TableView<Document> table;

    public AdminDocumentListController(DocumentService documentService, CatalogService catalogService) {
        this.documentService = documentService;
        this.catalogService = catalogService;
    }

    @FXML
    public void initialize() {
        updateTable();
    }

    @FXML
    void add(ActionEvent event) throws IOException {
        AddDocumentController.load();
    }

    @FXML
    void back(ActionEvent event) throws IOException {
        AdminCatalogController.load();
    }

    void updateTable() {
        TableColumn<Document, Document> idCol = new TableColumn<>("ID");
        idCol.setCellValueFactory(new PropertyValueFactory<>("id"));

        TableColumn<Document, Document> nameCol = new TableColumn<>("Название");
        nameCol.setCellValueFactory(new PropertyValueFactory<>("name"));

        TableColumn<Document, Document> descriptionCol = new TableColumn<>("Описание");
        descriptionCol.setCellValueFactory(new PropertyValueFactory<>("description"));

        TableColumn<Document, Document> dateCol = new TableColumn<>("Дата регистрации");
        dateCol.setCellValueFactory(new PropertyValueFactory<>("createdAt"));

        TableColumn<Document, Document> deleteCol = new TableColumn<>("");
        deleteCol.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue()));
        deleteCol.setCellFactory(param -> new TableCell<Document, Document>() {
            private final Button openButton = new Button("Удалить");

            @Override
            protected void updateItem(Document document, boolean empty) {
                super.updateItem(document, empty);

                if (document == null) {
                    setGraphic(null);
                    return;
                }

                setGraphic(openButton);
                openButton.setOnAction(event -> {
                    try {
                        documentService.deleteDocument(document.getId());
                        updateTable();
                    } catch (Exception e) {
                        e.printStackTrace();
                        PopupWindow.openWindow("Ошибка", "Запись не найдена");
                    }
                });
            }
        });

        TableColumn<Document, Document> saveCol = new TableColumn<>("");
        saveCol.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue()));
        saveCol.setCellFactory(param -> new TableCell<Document, Document>() {
            private final Button openButton = new Button("Скачать");

            @Override
            protected void updateItem(Document document, boolean empty) {
                super.updateItem(document, empty);

                if (document == null) {
                    setGraphic(null);
                    return;
                }

                setGraphic(openButton);
                openButton.setOnAction(event -> {
                    try {
                    DirectoryChooser chooser = new DirectoryChooser();
                    chooser.setTitle("Выберите папку");
                    File file = chooser.showDialog(Main.getStage());

                    if (file != null) {
                        if (!file.isDirectory()) {
                            PopupWindow.openWindow("Не является папкой");
                            return;
                        }

                        String savePath = file.getAbsoluteFile() + File.separator + document.getName() + "." + document.getExtension();

                        Files.write(Paths.get(savePath), document.getData());

                        PopupWindow.openWindow("Готово", "Готово");
                    }
                } catch (Exception e) {
                    PopupWindow.openWindow("Ошибка", "Запись не найдена");
                }
                });
            }
        });

        ObservableList<Document> values = FXCollections
                .observableArrayList(documentService.getDocuments(catalogService.getCurrentCatalog()));

        table.setItems(values);
        table.getColumns().clear();

        table.getColumns().addAll(
                idCol, nameCol,
                descriptionCol,
                dateCol, saveCol, deleteCol
        );
    }

    public static void load() throws IOException {
        FXMLLoader loader = new FXMLLoader(RegistrationController.class.getResource("/admin_documents.fxml"));
        loader.setControllerFactory(Main.getApplicationContext().getBeanFactory()::getBean);
        Parent view = loader.load();
        Main.getStage().setScene(new Scene(view));
        Main.getStage().show();
    }
}
