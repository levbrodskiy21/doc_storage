package com.lva.documentstorage.controller;

import com.lva.documentstorage.Main;
import com.lva.documentstorage.domain.enums.RoleName;
import com.lva.documentstorage.service.UserService;
import com.lva.documentstorage.service.exception.EntityAlreadyExistsException;
import com.lva.documentstorage.service.exception.EntityNotFoundException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;

import java.io.IOException;

@Controller
public class AuthenticationController {
    private final UserService userService;

    @FXML
    private TextField email;

    @FXML
    private TextField password;

    @Autowired
    public AuthenticationController(UserService userService) {
        this.userService = userService;
    }

    @FXML
    void register(ActionEvent event) throws IOException {
        RegistrationController.load();
    }

    @FXML
    void login(ActionEvent event) throws IOException {
        String login = email.getText();
        String pass = password.getText();

        if (StringUtils.isEmpty(login) || StringUtils.isEmpty(pass)) {
            PopupWindow.openWindow("", "");
            return;
        }

        try {
            userService.auth(login, pass);
        } catch (EntityNotFoundException e) {
            e.printStackTrace();
            PopupWindow.openWindow("Пользователь не найден");
            return;
        }

        RoleName role = userService.getCurrentUser().getRole().getName();
        System.out.println(userService.getCurrentUser());
        if (role == RoleName.ADMIN) {
            AdminCatalogController.load();
        } else if (role == RoleName.USER) {
            UserCatalogController.load();
        }
    }


    public static void load() throws IOException {
        FXMLLoader loader = new FXMLLoader(RegistrationController.class.getResource("/authentication.fxml"));
        loader.setControllerFactory(Main.getApplicationContext().getBeanFactory()::getBean);
        Parent view = loader.load();
        Main.getStage().setScene(new Scene(view));
        Main.getStage().show();
    }
}
