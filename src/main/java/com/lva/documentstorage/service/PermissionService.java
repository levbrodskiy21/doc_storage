package com.lva.documentstorage.service;

import com.lva.documentstorage.domain.Document;
import com.lva.documentstorage.domain.Permission;
import com.lva.documentstorage.domain.PermissionRequest;
import com.lva.documentstorage.domain.User;
import com.lva.documentstorage.repository.DocumentRepository;
import com.lva.documentstorage.repository.PermissionRepository;
import com.lva.documentstorage.repository.PermissionRequestRepository;
import com.lva.documentstorage.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class PermissionService {
    private final PermissionRepository permissionRepository;
    private final DocumentRepository documentRepository;
    private final UserRepository userRepository;
    private final PermissionRequestRepository permissionRequestRepository;

    @Autowired
    public PermissionService(PermissionRepository permissionRepository, DocumentRepository documentRepository, UserRepository userRepository, PermissionRequestRepository permissionRequestRepository) {
        this.permissionRepository = permissionRepository;
        this.documentRepository = documentRepository;
        this.userRepository = userRepository;
        this.permissionRequestRepository = permissionRequestRepository;
    }

    public List<Permission> getPermissions() {
        return permissionRepository.findAll();
    }

    @Transactional
    public void addPermission(Long userId, Long documentId) {
        Document document = documentRepository.findById(documentId).get();
        User user = userRepository.findById(userId).get();

        Permission permission = new Permission();
        permission.setId(null);
        permission.setDocument(document);
        permission.setUser(user);

        permissionRepository.save(permission);
    }

    @Transactional
    public boolean permissionExists(Long userId, Long documentId) {
        return permissionRepository.existsByDocument_IdAndUser_IdAndAcceptedIsTrue(documentId, userId);
    }

    @Transactional
    public void deletePermission(Long userId, Long documentId) {
        permissionRepository.deleteByDocument_IdAndUser_Id(documentId, userId);
    }

    @Transactional
    public void acceptPermission(Long permissionId) {
        Permission permission = permissionRepository.findById(permissionId).get();

        permission.setAccepted(true);

        permissionRepository.save(permission);
    }

    @Transactional
    public void deniedPermission(Long permissionId) {
        Permission permission = permissionRepository.findById(permissionId).get();

        permission.setAccepted(false);

        permissionRepository.save(permission);
    }

    @Transactional
    public void addPermissionRequest(PermissionRequest request) {
        Permission permission = new Permission();
        permission.setUser(request.getUser());
        permission.setDocument(request.getDocument());
        permission.setAccepted(false);

        permissionRepository.save(permission);
    }

    @Transactional
    public List<PermissionRequest> getPermissionRequests() {
        return permissionRequestRepository.findAll();
    }


}
