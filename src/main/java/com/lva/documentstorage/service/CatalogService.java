package com.lva.documentstorage.service;

import com.lva.documentstorage.domain.Catalog;
import com.lva.documentstorage.domain.User;
import com.lva.documentstorage.repository.CatalogRepository;
import com.lva.documentstorage.request.CatalogRequest;
import com.lva.documentstorage.service.exception.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CatalogService {
    private final CatalogRepository catalogRepository;
    private Catalog currentCatalog = null;

    @Autowired
    public CatalogService(CatalogRepository catalogRepository) {
        this.catalogRepository = catalogRepository;
    }

    public Catalog getCurrentCatalog() {
        return currentCatalog;
    }

    public void setCurrentCatalog(Catalog currentCatalog) {
        this.currentCatalog = currentCatalog;
    }

    @Transactional
    public Catalog addCatalog(CatalogRequest request) {
        Catalog catalog = new Catalog();
        catalog.setId(null);
        catalog.setTitle(request.getTitle());
        catalog.setDescription(request.getDescription());

        return catalogRepository.save(catalog);
    }

    @Transactional
    public Catalog updateCatalog(long catalogId, CatalogRequest request) throws EntityNotFoundException {
        Catalog catalog = catalogRepository.findById(catalogId)
                .orElseThrow(() -> new EntityNotFoundException(""));

        catalog.setTitle(request.getTitle());
        catalog.setDescription(request.getDescription());

        catalog = catalogRepository.save(catalog);

        return catalog;
    }

    @Transactional
    public void deleteCatalog(long catalogId) {
        catalogRepository.deleteById(catalogId);
    }

    @Transactional
    public List<Catalog> getCatalogs() {
        return catalogRepository.findAll();
    }
}
