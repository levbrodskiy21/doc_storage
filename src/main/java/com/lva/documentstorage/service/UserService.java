package com.lva.documentstorage.service;

import com.lva.documentstorage.domain.Role;
import com.lva.documentstorage.domain.User;
import com.lva.documentstorage.domain.enums.RoleName;
import com.lva.documentstorage.repository.RoleRepository;
import com.lva.documentstorage.repository.UserRepository;
import com.lva.documentstorage.service.exception.EntityAlreadyExistsException;
import com.lva.documentstorage.service.exception.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserService {
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;
    private User currentUser;

    @Autowired
    public UserService(UserRepository userRepository, RoleRepository roleRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Transactional
    public void register(String email, String password) throws EntityAlreadyExistsException {
        if (userRepository.existsByEmail(email)) {
            throw new EntityAlreadyExistsException();
        }

        User user = new User();
        user.setId(null);
        user.setEmail(email);
        user.setPassword(passwordEncoder.encode(password));

        Role role = roleRepository.findByName(RoleName.USER);

        user.setRole(role);

        currentUser = userRepository.save(user);
    }

    @Transactional
    public void auth(String email, String password) throws EntityNotFoundException {
        User user = userRepository.findByEmail(email)
                .orElseThrow(() -> new EntityNotFoundException(""));

        boolean isMatched = passwordEncoder.matches(password, user.getPassword());

        if (!isMatched) {
            throw new EntityNotFoundException("");
        }

        currentUser = user;
    }

    public User getCurrentUser() {
        return currentUser;
    }
}
