package com.lva.documentstorage.service;

import com.lva.documentstorage.domain.Catalog;
import com.lva.documentstorage.domain.Document;
import com.lva.documentstorage.domain.User;
import com.lva.documentstorage.domain.enums.RoleName;
import com.lva.documentstorage.repository.CatalogRepository;
import com.lva.documentstorage.repository.DocumentRepository;
import com.lva.documentstorage.repository.PermissionRepository;
import com.lva.documentstorage.request.DocumentRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@Service
public class DocumentService {
    private final DocumentRepository documentRepository;
    private final CatalogRepository catalogRepository;

    @Autowired
    public DocumentService(DocumentRepository documentRepository, CatalogRepository catalogRepository) {
        this.documentRepository = documentRepository;
        this.catalogRepository = catalogRepository;
    }

    @Transactional
    public void addDocument(Catalog catalog, DocumentRequest request) throws IOException {
        Document document = new Document();
        document.setId(null);
        document.setCatalog(catalog);
        document.setName(request.getName());
        document.setDescription(request.getDescription());

        Path path = Paths.get(request.getPath());

        if (!Files.exists(path)) {
            throw new IOException();
        }
        String extension = "";

        int i = request.getPath().lastIndexOf('.');

        if (i > 0) {
            extension = request.getPath().substring(i+1);
        }

        document.setExtension(extension);

        byte[] data = Files.readAllBytes(Paths.get(request.getPath()));

        document.setData(data);
        catalog.getDocuments().add(document);
        documentRepository.save(document);

        catalogRepository.save(catalog);
    }

    @Transactional
    public List<Document> getDocuments(User user) {
        if (user.getRole().getName() == RoleName.ADMIN) {
            return documentRepository.findAll();
        }

        return documentRepository.findAll();
    }

    @Transactional
    public List<Document> getDocuments(Catalog catalog) {
        return documentRepository.findAllByCatalog_Id(catalog.getId());
    }

    @Transactional
    public void deleteDocument(Long id) {
        documentRepository.deleteById(id);
    }
}
