package com.lva.documentstorage.domain.enums;

public enum UserStatus {
    ACTIVE, LOCKED
}
