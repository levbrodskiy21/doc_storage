package com.lva.documentstorage.domain.enums;

public enum RoleName {
    USER, ADMIN
}
