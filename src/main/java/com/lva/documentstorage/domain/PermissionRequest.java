package com.lva.documentstorage.domain;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "permission_requests")
public class PermissionRequest {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    private User user;
    @ManyToOne
    private Document document;
}
