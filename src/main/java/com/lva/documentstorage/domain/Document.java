package com.lva.documentstorage.domain;

import lombok.Data;
import org.springframework.data.repository.cdi.Eager;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;

@Data
@Entity
@Table(name = "documents")
public class Document {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String description;
    @Lob
    private byte[] data;
    @ManyToOne(fetch = FetchType.EAGER)
    private Catalog catalog;
    private String extension;
    @Column(name = "created_at")
    private LocalDate createdAt;

    @PrePersist
    private void prePersist() {
        createdAt = LocalDate.now();
    }

    @PreUpdate
    private void preUpdate() {
        createdAt = LocalDate.now();
    }
}
