package com.lva.documentstorage.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String email;
    private String password;
    @OneToOne
    private Role role;
    @Temporal(value = TemporalType.DATE)
    @Column(name = "created_at")
    private Date createdAt;

}
