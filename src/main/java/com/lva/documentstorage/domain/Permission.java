package com.lva.documentstorage.domain;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "permissions")
public class Permission {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @OneToOne
    private Document document;
    @OneToOne
    private User user;

    private Boolean accepted;
}
