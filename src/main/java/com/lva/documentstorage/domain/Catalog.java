package com.lva.documentstorage.domain;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Data
@Entity
@Table(name = "catalogs")
public class Catalog {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    private String description;
    @OneToMany(orphanRemoval = true, fetch = FetchType.EAGER)
    private List<Document> documents;
    @Column(name = "created_at")
    private LocalDate createdAt;

    @PrePersist
    private void prePersist() {
        createdAt = LocalDate.now();
    }

    @PreUpdate
    private void preUpdate() {
        createdAt = LocalDate.now();
    }
}
