package com.lva.documentstorage.domain;

import com.lva.documentstorage.domain.enums.CategoryName;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "categories")
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Enumerated(value = EnumType.STRING)
    private CategoryName name;
}
