package com.lva.documentstorage.repository;

import com.lva.documentstorage.domain.PermissionRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PermissionRequestRepository extends JpaRepository<PermissionRequest, Long> {
}
