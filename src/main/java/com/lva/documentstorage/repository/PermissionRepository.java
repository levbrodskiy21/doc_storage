package com.lva.documentstorage.repository;

import com.lva.documentstorage.domain.Permission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PermissionRepository extends JpaRepository<Permission, Long> {
    void deleteByDocument_IdAndUser_Id(Long documentId, Long userId);
    List<Permission> findByUser_Id(Long userId);
    boolean existsByDocument_IdAndUser_IdAndAcceptedIsTrue(Long documentId, Long userId);
}
