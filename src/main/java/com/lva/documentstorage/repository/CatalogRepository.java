package com.lva.documentstorage.repository;

import com.lva.documentstorage.domain.Catalog;
import com.lva.documentstorage.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CatalogRepository extends JpaRepository<Catalog, Long> {

}
