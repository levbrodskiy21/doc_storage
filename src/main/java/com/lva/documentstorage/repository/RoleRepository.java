package com.lva.documentstorage.repository;

import com.lva.documentstorage.domain.Role;
import com.lva.documentstorage.domain.enums.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Role findByName(RoleName name);
    Optional<Role> findRoleByName(RoleName name);
}
