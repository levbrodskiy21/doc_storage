package com.lva.documentstorage.config;

import com.lva.documentstorage.domain.Role;
import com.lva.documentstorage.domain.User;
import com.lva.documentstorage.domain.enums.RoleName;
import com.lva.documentstorage.repository.RoleRepository;
import com.lva.documentstorage.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Optional;

@Component
public class MainConfiguration {
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public MainConfiguration(UserRepository userRepository, RoleRepository roleRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @PostConstruct
    private void init() {
        Optional<Role> role1 = roleRepository.findRoleByName(RoleName.ADMIN);
        Optional<Role> role2 = roleRepository.findRoleByName(RoleName.USER);

        if (!role1.isPresent()) {
            Role role = new Role();
            role.setName(RoleName.ADMIN);
            roleRepository.save(role);
        }

        if (!role2.isPresent()) {
            Role role = new Role();
            role.setName(RoleName.USER);
            roleRepository.save(role);
        }

        User admin = userRepository.findByEmail("admin").orElse(null);

        if (admin != null) {
            return;
        }

        admin = new User();
        admin.setEmail("admin");
        admin.setPassword(passwordEncoder.encode("admin"));
        admin.setRole(roleRepository.findByName(RoleName.ADMIN));
        userRepository.save(admin);

        User user = userRepository.findByEmail("user").orElse(null);

        if (user != null) {
            return;
        }

        user = new User();
        user.setEmail("user");
        user.setPassword(passwordEncoder.encode("user"));
        user.setRole(roleRepository.findByName(RoleName.USER));
        userRepository.save(user);
    }
}
